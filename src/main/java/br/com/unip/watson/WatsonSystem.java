package br.com.unip.watson;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Builder
@AllArgsConstructor
@Getter
public class WatsonSystem implements Serializable {
	private static final long serialVersionUID = -9155253803919974293L;

	private boolean initialized;
	
	private List<DialogStack> dialog_stack;
	
	private Integer dialog_turn_counter;
	
	private Integer dialog_request_counter;
	
	private Object _node_output_map;
	
	private String last_branch_node;
	
	private boolean branch_exited;
	
	private String branch_exited_reason;
	
}
