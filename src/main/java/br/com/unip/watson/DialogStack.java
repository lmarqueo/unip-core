package br.com.unip.watson;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class DialogStack implements Serializable {
	private static final long serialVersionUID = 1476808624511510383L;

	private String dialog_node;
	
}
