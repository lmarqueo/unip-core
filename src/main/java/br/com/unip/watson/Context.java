package br.com.unip.watson;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter  
@ToString
public class Context implements Serializable {
	private static final long serialVersionUID = 8675591586185546137L;

	private String conversation_id;

    private WatsonSystem system;

}
