package br.com.unip.watson.request;

import java.io.Serializable;

import br.com.unip.dto.MessageDTO;
import br.com.unip.watson.Context;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@ToString
public class ConversationResponse implements Serializable {
	private static final long serialVersionUID = 172354469020688068L;

	private MessageDTO message;
	
	private Context context;
	
}
