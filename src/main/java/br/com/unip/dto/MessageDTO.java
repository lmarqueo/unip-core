package br.com.unip.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter 
@ToString
public class MessageDTO implements Serializable {
	private static final long serialVersionUID = 8186089644135295300L;

	private UserDTO user;
	
	private String content;
	
	private LocalDateTime date;
	
}
