package br.com.unip.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class UserDTO implements Serializable {
	private static final long serialVersionUID = 1983003288375877127L;

	@Setter
	private Long id;

    private String first_name;

    private String last_name;

    private String profile_pic;

    private String email;
	
}
